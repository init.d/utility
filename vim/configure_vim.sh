#!/bin/bash

set -e

# Define section
VUNDLE_GIT_REPO=https://github.com/VundleVim/Vundle.vim.git
VUNDLE_LOCATION=~/.vim/bundle/Vundle.vim
VUNDLE_CONFIG_TEMPLATE=./vimrc.conf.template
VUNDLE_CONFIG=~/.vimrc
VUNDLE_CONFIG_BACKUP=~/._vimrc.backup
SCRIPT_PREFIX="vim ::"
# End define section


# Run
echo "$SCRIPT_PREFIX Install Vundle ..."
if [ -d $VUNDLE_LOCATION ]; then
	echo "$SCRIPT_PREFIX Install Vundle ...OK (already installed)"
else
    git clone $VUNDLE_GIT_REPO $VUNDLE_LOCATION > /dev/null 2>&1
    echo "${SCRIPT_PREFIX} Install Vundle ...OK"
fi

if [ -f $VUNDLE_CONFIG ]; then
    cp $VUNDLE_CONFIG $VUNDLE_CONFIG_BACKUP
    echo "$SCRIPT_PREFIX Backup current vim config ...OK"
fi

cp $VUNDLE_CONFIG_TEMPLATE $VUNDLE_CONFIG
echo "$SCRIPT_PREFIX Copy vim config ...OK"

# Install plugins over Vundle
echo "$SCRIPT_PREFIX Install vim plugins ..."
echo | echo | vim +PluginInstall +qall &>/dev/null
if [ `eval echo $?` -eq 0 ]; then
    echo "$SCRIPT_PREFIX Install vim plugins ...OK"
else
    echo "$SCRIPT_PREFIX Install vim plugins ...FAIL"
fi
